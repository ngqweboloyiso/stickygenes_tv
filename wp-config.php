<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'skinnygenes_tv');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', 'mysql');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'v`5LV(ic;d-U`#+oIH`;zkGiAoRnk_5R*[%d#qE].BdP2qZ!EZd#Su;EX~&Yb^bP');
define('SECURE_AUTH_KEY',  ' +Y7L-_Ac;FU}Op_<O1H8!7+TpTYlq>ygWl^D/,b]:DaGk2gW7W6~%[d3 7qyO;$');
define('LOGGED_IN_KEY',    'Zcv*+j},}ZUBjGS!D-Vy44.Fu5_Ncl*i276ZwwT1C1KF8Tkx}?{+&_`2T&K$|>cz');
define('NONCE_KEY',        '`pie~pR#)wv4MwaC~@_@fkNB$ro t6W2zJ&n~DT| _Sc0ta#cvo])@]N`4BrK9{#');
define('AUTH_SALT',        'LbQRD;ZwC58@h(^{s+QuLoln(RY=eFyjAK4jmT0d%:|iRTO+F0N&r1,n|m6ThYk{');
define('SECURE_AUTH_SALT', '?_hj)Jq_o3W=}4-4.%}*!|<lyB-3Ml1+jv@z;dPx;a?rPkB,K0.2CdygH::n,R3d');
define('LOGGED_IN_SALT',   '0ZT{5V M |$*ig1rGKl8)tQ}a.u1RG;-rCYlw-Gg8^5k$2!|?Lz<gh>ga*qB{3d0');
define('NONCE_SALT',       '$Sf$p 4eu<o!,2g @9=A{5flv9eBCNWj&aFn8Kq%1&(0V9!JhIg<J[8D`vI.^9%A');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
