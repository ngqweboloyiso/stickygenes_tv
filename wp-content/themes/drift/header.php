<?php
/**
 * The Header for our theme.
 *
 * Displays all of the <head> section and everything up until id="main-core".
 *
 * @package ThinkUpThemes
 */
?><!DOCTYPE html>

<html <?php language_attributes(); ?>>
<head>
<meta charset="<?php bloginfo( 'charset' ); ?>" />
<meta name="viewport" content="width=device-width" />
<link rel="profile" href="//gmpg.org/xfn/11" />
<link rel="pingback" href="<?php esc_url( bloginfo( 'pingback_url' ) ); ?>" />

<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>
<div class="site-wrapper">
<div id="body-core-wrap">
<div id="body-core-header">
<!-- Social Icons -->
	<div class="social-media-top">
	<ul>
	<li><a href="<?php echo site_url(); ?>"><img src="<?php echo get_template_directory_uri(); ?>/images/social/facebook_icon.jpg"></a></li>
	<li><a href="<?php echo site_url(); ?>"><img src="<?php echo get_template_directory_uri(); ?>/images/social/twitter_icon.jpg"></a></li>
	<li><a href="<?php echo site_url(); ?>"><img src="<?php echo get_template_directory_uri(); ?>/images/social/instagram_icon.jpg"></a></li>
	<li><a href="<?php echo site_url(); ?>"><img src="<?php echo get_template_directory_uri(); ?>/images/social/youtube_icon.jpg"></a></li>
	</ul>
	</div>
	<!-- End Social Icons -->
		


		<div id="pre-header">
		<div class="wrap-safari">
		<div id="pre-header-core" class="main-navigation">

			<?php if ( has_nav_menu( 'pre_header_menu' ) ) : ?>
			<?php drift_thinkup_input_preheaderhtml(); ?>
			<?php endif; ?>

		</div>
		</div>
		</div>
		<!-- #pre-header -->

		<?php drift_thinkup_input_headersearch(); ?>

		<div id="header">
		<div id="header-core">

			<div id="header-links" class="main-navigation">
			<div id="header-links-inner" class="header-links">

				<?php $walker = new drift_thinkup_menudescription;
				wp_nav_menu(array( 'container' => false, 'theme_location'  => 'header_menu', 'walker' => new drift_thinkup_menudescription() ) ); ?>

			</div>
			</div>
			<!-- #header-links .main-navigation -->
 
			<?php /* Social Media Icons */ drift_thinkup_input_socialmediaheadermain(); ?>

		</div>
		</div>
		<!-- #header -->

		<?php /* Add responsive header menu */ drift_thinkup_input_responsivehtml1(); ?>
		<!-- #header-nav -->
		<form action="/" method="get" class="search-form">
    		<input type="text" name="s" id="search-header" value="<?php the_search_query(); ?>" />
    		<input type="submit" value="Search" id="header-search-button">
		</form>
</div>

<div id="body-core">

	<header>

	<div id="site-header">

		<?php if ( get_header_image() ) : ?>
			<div class="custom-header"><img src="<?php header_image(); ?>" width="<?php echo esc_attr( get_custom_header()->width ); ?>" height="<?php echo esc_attr( get_custom_header()->height ); ?>" alt="">

			</div>
		<?php endif; // End header image check. ?>

		<?php /* Add responsive header menu */ drift_thinkup_input_responsivehtml2(); ?>

		<div class="headerslider"> <?php if (is_front_page()) { echo do_shortcode('[metaslider id=11]'); } ?></div>
				<div class="skinnylogo"><a href="<?php echo site_url(); ?>">SkinnyGenes.<span>tv</span></a></div>
		<?php /* Custom Intro - Below */ drift_thinkup_custom_intro(); ?>


	</div>

	</header>
	<!-- header -->

	<?php /*  Call To Action - Intro */ drift_thinkup_input_ctaintro(); ?>
	<?php /*  Pre-Designed HomePage Content */ drift_thinkup_input_homepagesection(); ?>

	<div id="content">
	<div id="content-core">

		<div id="main">
		<div id="main-core">