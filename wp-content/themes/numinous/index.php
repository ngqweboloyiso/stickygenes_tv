<?php
/**
 * The main template file.
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * E.g., it puts together the home page when no home.php file exists.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package Numinous
 */

get_header(); ?>

<div id="primary" class="content-area">
	<main id="main" class="site-main" role="main">
	<!-- end slider -->
		<!-- <h3>Latest</h3> -->
			 <div class="col-lg-12">
				<?php masterslider(3); ?>

 			</div> 
<!-- end slider -->
				
 				<div class="col-lg-12 news-section">
 				<div class="row">
 				<!-- 	<h3>Exclusive Video</h3> -->
							<?php echo do_shortcode("[pt_view id=f9bf77fkn5]"); ?>
 				</div>
 				</div>
		</main><!-- #main -->
	</div><!-- #primary -->
<?php
get_sidebar();
get_footer();
